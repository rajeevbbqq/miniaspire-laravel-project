<?php

/*
|--------------------------------------------------------------------------
| Documentation available
|--------------------------------------------------------------------------
|
| Please read the documentation by using this link
| 
| 
|
*/

/*
|--------------------------------------------------------------------------
| User creation routes
|--------------------------------------------------------------------------
|
| These routes are not required with any headers or tokens.
| These routes are used for creating users.
|
*/


Route::post('user/create', 'UserController@create');
Route::post('user/resend', 'UserController@resend');

Route::post('user/verify', 'UserController@verify');
Route::post('user/login', 'UserController@login');

Route::post('user/forget', 'UserController@forget');
Route::post('user/password/change', 'UserController@ChangePassword');


/*
|--------------------------------------------------------------------------
| Authenticated Routes
|--------------------------------------------------------------------------
|
| These routes requires Bearer token on header. Once a user can successfully sign-in
| he gets an "auth-token", has to be passed as Bearer token
| 
|
*/


Route::middleware(['validUser'])->group(function () {        

	Route::post('application/start', 'TransactionController@create');
	Route::post('application/{id}/profile/save', 'TransactionController@profile');
	Route::post('application/{id}/employment/save', 'TransactionController@employment');
	Route::post('application/{id}/documents/save', 'TransactionController@documents');
	Route::post('application/{id}/submit', 'TransactionController@submit');


	Route::post('loan/{id}/payment', 'PaymentController@create');

});

/*
|--------------------------------------------------------------------------
| Loan Routes
|--------------------------------------------------------------------------
|
| These routes are designed for responding to submitted applications.
| These routes has to be handled from finance team.
| 
|
*/

Route::post('loan/{id}/callback', 'LoanController@callback');

Route::post('loan/{id}/disburse', 'LoanController@disburse');

Route::get('loan/{id}/details', 'LoanController@show');


