<?php

namespace Aspire\Events;

use Illuminate\Queue\SerializesModels;

use Aspire\User;

class GenerateToken
{
    use SerializesModels;

    public $user;
    public $type;

    public function __construct(User $user, $type)
    {
        $this->user = $user;
        $this->type = $type;
    }
}
