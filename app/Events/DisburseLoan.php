<?php

namespace Aspire\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;

use Aspire\Transaction;

class DisburseLoan
{
    use Dispatchable, SerializesModels;

    public $transaction;
    public $interest;
    public $processing_fee;

    public function __construct(Transaction $transaction, $interest, $processing_fee)
    {
        $this->transaction    = $transaction;
        $this->interest       = $interest;
        $this->processing_fee = $processing_fee;
    }
}
