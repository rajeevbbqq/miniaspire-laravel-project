<?php

namespace Aspire\Listeners;

use Aspire\Events\DisburseLoan;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Aspire\Loan;

class LoanDisbursement
{
    public function handle(DisburseLoan $event)
    {
        $t = $event->transaction; // Transaction assigned to $t

        // Approved amount  = Applied amount
        // Disbursed amount = Applied amount - Processing Fee

        $disbursed_amount = $t->loan_amount - $event->processing_fee;

        $emi_details = $this->calculteEmi( 
                                           $t->loan_amount, 
                                           $event->interest, 
                                           $t->tenure_in_months
                                         );

        $emi_details      = collect($emi_details);

        $interest_payable = round($emi_details->sum('month_interest'), 2);
        $amount_payable   = $t->loan_amount + $interest_payable;
        $monthly_emi      = round(($amount_payable / $t->tenure_in_months));

        $application_date = now()->parse($t->created_at)->format('Y-m-d');

        $loan_start_date  = $emi_details->first()['due_date'];
        $loan_end_date    = $emi_details->last()['due_date'];

        $loan_number = $this->generateLoanId();

        $loan_details = [
            'loan_number'      => $loan_number,
            'applied_amount'   => $t->loan_amount,
            'approved_amount'  => $t->loan_amount,
            'disbursed_amount' => $disbursed_amount,
            'processing_fee'   => $event->processing_fee,
            'interest'         => $event->interest, 
            'interest_payable' => $interest_payable,
            'amount_payable'   => $amount_payable,
            'monthly_emi'      => $monthly_emi,
            'tenure'           => $t->tenure_in_months,
            'application_date' => $application_date,
            'loan_start_date'  => $loan_start_date,
            'loan_end_date'    => $loan_end_date
        ];


        $emi_details = $emi_details->toArray();  // Converting EMI object to array

        $loan = $t->loan()->create($loan_details); // registering loan
        $loan->emis()->createMany($emi_details); // creating emi details table

        $t->status = 'LOAN_DISBURSED';
        $t->save();

        return $loan_details;
    }

    private function calculteEmi($loan_amount, $interest, $tenure)
    {
        $total_interest = 0 ;

        for ($i=1; $i <= $tenure; $i++) 
        { 
            $due_date = now()->addMonth($i)->format('Y-m-05'); // Every 5th of the month

            $month_interest = ((($loan_amount * $interest)/100)/12);

            $total_interest += $month_interest;

            $loan_amount += $month_interest;

            $calculated[] = [
                'balance_amount' => round($loan_amount, 2),
                'month_interest' => round($month_interest, 2),
                'total_interest' => round($total_interest, 2),
                'due_date'       => $due_date,
                'term'           => $i
            ];
        }

        return $calculated;
    }

    private function generateLoanId()
    {
        $run = true;

        while ($run) 
        {
            $loan_number = 'AC'.strtoupper(str_random(12));

            $exist = Loan::where('loan_number', $loan_number)->exists();

            if (!$exist) 
            {
                $run = false;
            }
        }

        return $loan_number;
    }
}
