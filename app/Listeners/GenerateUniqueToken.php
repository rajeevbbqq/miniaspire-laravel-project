<?php

namespace Aspire\Listeners;

use Aspire\Events\GenerateToken;

use Aspire\Token;

class GenerateUniqueToken
{
    public function handle(GenerateToken $event)
    {
         $type = strtolower($event->type);
        $token = $this->createToken($type);

        $run = true;
        while ($run) 
        {
            $data = [
                        'type'  => $type,
                        'token' => $token
                    ];

            $exist = Token::where($data)->exists();

            if (!$exist) 
            {
                $run = false;
                $u = $event->user;

                if ($type == 'session') 
                {
                    $u->token()->create($data);
                }
                else
                {                    
                    $u->token()->updateOrCreate(
                        ['type'  => $type],
                        [
                            'token'      => $token,
                            'created_at' => now()
                        ]
                    );
                }                
            }
        }

        return $token;
    }

    private function createToken($tokenType)
    {
        if ($tokenType == 'session') 
        {
            $token = str_random(36);
        }
        else
        {
            $token = random_int(10000, 99999); // default token size (5)
        }

        return $token;
    }
}
