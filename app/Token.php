<?php

namespace Aspire;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    const UPDATED_AT = null;

    protected $guarded = ['id'];

    public function user()
    {
    	return $this->belongsTo(User::class);
    }
}
