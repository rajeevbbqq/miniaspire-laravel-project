<?php

namespace Aspire;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $guarded = ['id', 'transaction_id'];

    protected $hidden = ['id', 'transaction_id'];

    protected $casts = [
    	'application_date' => 'date:Y-m-d',
    	'loan_start_date'  => 'date:Y-m-d',
    	'loan_end_date'    => 'date:Y-m-d'
	];

	public function payment()
	{
		return $this->hasMany(Payment::class);
	}

	public function transaction()
	{
		return $this->belongsTo(Transaction::class);
	}

	public function emis()
	{
		return $this->hasMany(EmiDetails::class);
	}
}
