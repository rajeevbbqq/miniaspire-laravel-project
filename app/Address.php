<?php

namespace Aspire;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $guarded = ['id', 'transaction_id'];
}
