<?php

namespace Aspire;

use Illuminate\Database\Eloquent\Model;

class Upload extends Model
{
    protected $guarded = ['id', 'transaction_id'];

    protected $hidden = ['transaction_id', 'id'];
}
