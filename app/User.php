<?php

namespace Aspire;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $guarded = ['id'];

    protected $hidden = ['id', 'password'];

    public function token()
    {
    	return $this->hasMany(Token::class);
    }

    public function transaction()
    {
    	return $this->hasMany(Transaction::class);
    }

    public function loans()
    {
        return $this->hasManyThrough(Loan::class, Transaction::class);
    }
}
