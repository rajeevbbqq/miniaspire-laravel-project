<?php

namespace Aspire;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = ['id', 'loan_id'];

    protected $hidden = ['id', 'loan_id'];

    public function loan()
	{
		return $this->belongsTo(Loan::class);
	}
}
