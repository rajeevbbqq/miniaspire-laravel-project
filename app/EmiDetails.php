<?php

namespace Aspire;

use Illuminate\Database\Eloquent\Model;

class EmiDetails extends Model
{
    protected $guarded = ['id', 'loan_id'];

    protected $casts = [
    	'due_date' => 'date:Y-m-d'
	];

	protected $hidden = [
		'loan_id',
		'id'
	];
}
