<?php

namespace Aspire\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use Aspire\Transaction;
use Aspire\Loan;

use Event;
use Aspire\Events\DisburseLoan;

class LoanController extends Controller
{
	public function show(Request $request, $id)
	{
		$loan = Loan::where('loan_number', $id)
				    ->first();

		if (is_null($loan)) 
		{
			$code = 404;
			$data = [
			    		'code'     => $code,
			    		'response' => 'Application not found'
			    	];
		}
		else
		{
			$loan->load([
				'transaction.user', 'transaction.upload',
				'payment', 'emis'
			]);

			$code = 200;
			$data = [
			    		'code'     => $code,
			    		'response' => 'Loan application found',
			    		'data'     => $loan
			    	];
		}

		return response()->json($data, $code, [], JSON_PRETTY_PRINT);
	}

    public function callback(Request $request, $id)
    {
        	$t = Transaction::where([
        				'application_id' => $id
        		 ])->first();

        if (!is_null($t)) 
        {
        	$validator = Validator::make($request->all(), [
	        	'status' => 'required|in:DOCUMENTS_APPROVED,APPLICATION_REJECTED,APPLICATION_CANCELLED'
	        ]);

	        if ($validator->fails())
	        {
	        	foreach ($validator->errors()->all() as $error) 
		        {
		        	$code = 404;
		        	$data = [
		        		'code'     => $code,
		        		'response' => $error
		        	];
				}
	        }
	        else
	        {
	        	if ($t->status == 'DOCUMENTS_APPROVED' || $t->status == 'LOAN_DISBURSED') 
	        	{
	        		$code = 406;
			    	$data = [
			    		'code'     => $code,
			    		'response' => 'Loan already approved / disbursed'
			    	];
	        	}
	        	elseif ($t->status == 'APPLICATION_REJECTED') 
	        	{
	        		$code = 406;
			    	$data = [
			    		'code'     => $code,
			    		'response' => 'Application already rejected'
			    	];
	        	}
	        	elseif ($t->status != 'APPLICATION_SUBMITTED') 
	        	{
	        		$code = 406;
			    	$data = [
			    		'code'     => $code,
			    		'response' => 'Incomplete Application'
			    	];
	        	}
	        	else
	        	{
	        		$t->status = strtoupper($request->status);
	        		$t->save();

			    	$code = 200;
			    	$data = [
			    		'code'     => $code,
			    		'response' => 'Application updated sccessfully'
			    	];
	        	}
	        }        	
        }
        else
        {
        	$code = 404;
		    $data = [
		    		'code'     => $code,
		    		'response' => 'Invalid loan application'
		    	];
        }

    	return response()->json($data, $code);
    }


    public function disburse(Request $request, $id)
    {
    	$t = Transaction::where([
    						'application_id' => $id,
    						'status' => 'DOCUMENTS_APPROVED'
    					])
    				    ->first();

    	if (is_null($t)) 
    	{
    		$code = 404;
		    $data = [
		        		'code'     => $code,
		        		'response' => 'Invalid application'
		        	];
    	}
    	else
    	{
    		$validator = Validator::make($request->all(), [
	        	'interest'       => 'required|digits_between:1,2',
	        	'processing_fee' => 'required|numeric'
	        ]);

	        if ($validator->fails())
	        {
	        	foreach ($validator->errors()->all() as $error) 
		        {
		        	$code = 404;
		        	$data = [
		        		'code'     => $code,
		        		'response' => $error
		        	];
				}
	        }
	        else
	        {
	        	$interest       = $request->interest;
	        	$processing_fee = $request->processing_fee;

	        	$e = event(new DisburseLoan($t, $interest, $processing_fee));
	        	// Disburse loan against this transaction

	        	$code = 200;
		        $data = [
		        		'code'     => $code,
		        		'response' => 'Loan disbursed',
		        		'data'     => $e
		        	];
	        }
    	}

        return response()->json($data, $code);
    }
}
