<?php

namespace Aspire\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Hash;

use Event;
use Aspire\Events\GenerateToken;

use Aspire\User;
use Aspire\Token;

class UserController extends Controller
{
    public function create(Request $request)
    {
        $inputs = $request->only([
                        'email', 'password', 'name'
                  ]);

        $validator = Validator::make($inputs, [
            'email'    => 'bail|required|email|max:255|unique:users',
            'password' => 'bail|required|min:6|max:255',
            'name'     => 'bail|required|min:4|max:255'
        ]);

        if ($validator->fails())
        {
        	foreach ($validator->errors()->all() as $error) 
	        {
	        	$code = 404;
	        	$data = [
	        		'code'     => $code,
	        		'response' => $error
	        	];
			}
        }
        else
        {
            $inputs['password'] = $this->HashPassword($inputs['password']);

            $user = User::create($inputs);            
            $e = event(new GenerateToken($user, 'registration'))[0];

        	$code = 201;
	        $data = [
	        		'code'     => $code,
	        		'response' => 'Verify your email by entering OTP',
                    'sent' => [
                        'token' => $e,
                        'note' => 'We are not triggering any email'
                    ]
	        	];
        }

        return response()->json($data, $code);
    }

    public function verify(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'token' => 'bail|required|min:4|max:6'
        ]);

        if ($validator->fails())
        {
            foreach ($validator->errors()->all() as $error) 
            {
                $code = 404;
                $data = [
                    'code'     => $code,
                    'response' => $error
                ];
            }
        }
        else
        {
            $where = [
                'type'  => 'registration',
                'token' => $request->token
            ];

            $time = now()->subMinutes(15)->toDateTimeString(); 
                    // token is valid for 15 minutes

            $token = Token::where($where)->where('created_at', '>=', $time)->first();

            if (is_null($token)) 
            {
                $code = 404;
                $data = [
                    'code'     => $code,
                    'response' => 'Entered OTP is invalid / expired'
                ];
            }
            else
            {
                $token->user()->update([
                    'is_active' => true
                ]);

                $token->delete(); // deletes token after verification

                $code = 200;
                $data = [
                        'code'     => $code,
                        'response' => 'OTP verified, Login to start application'
                    ];
            }
            
        }

        return response()->json($data, $code);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'    => 'bail|required|email|exists:users',
            'password' => 'bail|required|min:6|max:255'
        ]);

        if ($validator->fails())
        {
            foreach ($validator->errors()->all() as $error) 
            {
                $code = 404;
                $data = [
                    'code'     => $code,
                    'response' => $error
                ];
            }
        }
        else
        {
            $where = [
                'email'     => $request->email,
                'is_active' => true
            ];

            $u = User::where($where)->first();

            if (is_null($u)) 
            {
                $code = 406;
                $data = [
                            'code'     => $code,
                            'response' => 'Please verify email first'
                        ];
            }
            else
            {
                if ($this->ValidatePassword($request->password, $u->password)) 
                { 
                    // Password is valid
                    $auth_token = event(new GenerateToken($u, 'session'))[0];
                    $code = 200;
                    $data = [
                                'code'     => $code,
                                'response' => 'Authenticated',
                                'data'     => [
                                    'auth_token' => $auth_token
                                ]
                            ];
                }
                else
                {
                    // Password is invalid
                    $code = 401;
                    $data = [
                                'code'     => $code,
                                'response' => 'Invalid password'
                            ];
                }                
            }
            
        }

        return response()->json($data, $code);
    }

    public function resend(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'bail|required|email|exists:users'
        ]);

        if ($validator->fails())
        {
            foreach ($validator->errors()->all() as $error) 
            {
                $code = 404;
                $data = [
                    'code'     => $code,
                    'response' => $error
                ];
            }
        }
        else
        {
            $where = [
                'email'     => $request->email,
                'is_active' => false
            ];

            $u = User::where($where)->first();

            if (is_null($u)) 
            {
                $code = 406;
                $data = [
                        'code'     => $code,
                        'response' => 'Email already verified'
                    ];
            }
            else
            {
                event(new GenerateToken($u, 'registration'));

                $code = 200;
                $data = [
                        'code'     => $code,
                        'response' => 'Verify your email by entering OTP'
                    ];
            }            
        }

        return response()->json($data, $code);
    }

    public function forget(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'bail|required|email|exists:users'
        ]);

        if ($validator->fails())
        {
            foreach ($validator->errors()->all() as $error) 
            {
                $code = 404;
                $data = [
                    'code'     => $code,
                    'response' => $error
                ];
            }
        }
        else
        {
            $where = [
                'email'     => $request->email,
                'is_active' => true
            ];

            $u = User::where($where)->first();

            if (is_null($u)) 
            {
                $code = 406;
                $data = [
                        'code'     => $code,
                        'response' => 'Please verify email first'
                    ];
            }
            else
            {
                event(new GenerateToken($u, 'forget'));
                
                $code = 200;
                $data = [
                        'code'     => $code,
                        'response' => 'Reset your password by entering OTP sent to email'
                    ];
            }            
        }

        return response()->json($data, $code);
    }

    public function ChangePassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'bail|required|email|exists:users',
            'token' => 'bail|required|exists:tokens|min:4|max:6',
            'password' => 'required|min:6|max:255'
        ]);

        if ($validator->fails())
        {
            foreach ($validator->errors()->all() as $error) 
            {
                $code = 404;
                $data = [
                    'code'     => $code,
                    'response' => $error
                ];
            }
        }
        else
        {         
            $where = [
                'email'     => $request->email,
                'is_active' => true
            ];

            $time = now()->subMinutes(15)->toDateTimeString();
            // token is valid for 15 minutes

            $u = User::where($where)
                     ->whereHas('token',function ($query) use ($request, $time) {
                            $query->where('token', $request->token)
                                  ->where('created_at', '>=', $time)
                                  ->where('type', 'forget');
                     })
                     ->first();

            if (is_null($u)) 
            {
                $code = 401;
                $data = [
                            'code'     => $code,
                            'response' => 'Invalid or expired OTP'
                        ];
            }    
            else
            {
                $u->password = $this->HashPassword($request->password);
                $u->save();

                $u->token()->delete();

                $code = 200;
                $data = [
                            'code'     => $code,
                            'response' => 'Password changed successfully'
                        ];
            }        
        }

        return response()->json($data, $code);
    }

    private function ValidatePassword($password, $hashedPassword)
    {
        if (Hash::check($password, $hashedPassword)) 
        {
            // Password matches
            return true;
        }

        // Entered password is invalid
        return false;
    }

    private function HashPassword($password)
    {
        return Hash::make($password);;
    }
}
