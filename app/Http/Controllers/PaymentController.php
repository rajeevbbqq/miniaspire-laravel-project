<?php

namespace Aspire\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

use Aspire\Loan;
use Aspire\Payment;

class PaymentController extends Controller
{
    public function create(Request $request, $id)
    {
    	$validator = Validator::make($request->all(), [
	        	'amount_paid'       => 'required|numeric',
	        	'paid_from'         => 'bail|required|date|date_format:Y-m-d',
	        	'paid_upto'         => 'bail|required|date|date_format:Y-m-d|after:paid_from'
	    ]);

	    if ($validator->fails())
	    {
	        	foreach ($validator->errors()->all() as $error) 
		        {
		        	$code = 404;
		        	$data = [
		        		'code'     => $code,
		        		'response' => $error
		        	];
				}
	    }
	    else
	    {
	    	$user_id = $request->get('user_id');

	    	$loan = Loan::whereHas('transaction.user', function($q) use ($user_id) {
				    	return $q->where('id', $user_id);
					})
		    		->where('loan_number', $id)
		    		->first();

		    if (is_null($loan)) 
		    {
		    	$code = 404;
			    $data = [
			        		'code'     => $code,
			        		'response' => 'Invalid application'
			        	];
		    }
		    else
		    {
		    	$dates = [$request->paid_from, $request->paid_upto];

		    	$loan->load(['emis' => function ($q) use ($dates) {
				    $q->where('is_paid', 'unpaid')
				      ->whereBetween('due_date', $dates);
				}]);

		    	// Counting number of payments in the given range
				$number_of_payments = collect($loan->emis)->count();

				if ($number_of_payments > 0) 
				{
					// Calculating for the given range
					$expected_amount = $number_of_payments * $loan->monthly_emi;

					if ($expected_amount != $request->amount_paid) 
					{
						$code = 406;
					    $data = [
					        		'code'     => $code,
					        		'response' => 'Amount cannot accept, please pay USD.'
					        		.$expected_amount
					        	];
					}
					else
					{
						$payment_reference = $this->generatePaymentId();

						$loan->payment()->create([
							'payment_reference' => $payment_reference,
							'amount_paid'       => $request->amount_paid,
							'paid_from'         => $request->paid_from,
							'paid_upto'         => $request->paid_upto
						]);

						$loan->emis()->whereBetween('due_date', $dates)
							 ->update(['is_paid' => 'paid']);

						$payment_details = [
							'date'              => now()->toDateTimeString(),
							'payment_reference' => $payment_reference,
							'emi_for_the_given_range' => $expected_amount,
							'amount_paid' => $request->amount_paid,
							'emi_details' => [
								'paid_from'   => $request->paid_from,
								'paid_upto'   => $request->paid_upto,							
								'monthly_emi' => $loan->monthly_emi,
								'emi_count'   => $number_of_payments
							]
						];

						$code = 200;
					    $data = [
					        		'code'     => $code,
					        		'response' => 'Payment accepted successfully',
					        		'data'     => $payment_details
					        	];
					}
				}
				else
				{
					$code = 404;
					$data = [
					        	'code'     => $code,
					        	'response' => 'There is no payments pending between this range'
					        	];
				}
		    }
	    }

    	return response()->json($data, $code);
    }


    private function generatePaymentId()
    {
    	$run = true;

    	while ($run) 
    	{
    		$payment_reference = strtoupper(str_random(14));

    		$exist = Payment::where('payment_reference', $payment_reference)
    							->exists();

    		if (!$exist) 
    		{
    			$run = false;
    		}
    	}

    	return $payment_reference;
    }
}
