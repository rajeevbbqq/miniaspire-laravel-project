<?php

namespace Aspire\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;

use Aspire\Transaction;
use Aspire\User;

class TransactionController extends Controller
{
	public function submit(Request $request, $id)
	{
		$user_id = $request->get('user_id');

        $t = Transaction::whereHas('user',function ($query) use ($user_id) {
                    $query->where('id',$user_id);
                 })
        		 ->where('application_id', $id)
        		 ->first();

        if (is_null($t)) 
        {
            $code = 404;
            $data = [
                    'code'     => $code,
                    'response' => 'Invalid application'
                ];
        }
        else
        {
            $accept = $this->canAccept($t->status);

            if ($accept['code'] != 200) 
            {
                $code = $accept['code'];
                $data = [
                        'code'     => $code,
                        'response' => $accept['status']
                    ];
            }
            else
            {
                $t->status = 'APPLICATION_SUBMITTED';
                $t->save();

                $code = 201;
                $data = [
                        'code'     => $code,
                        'response' => 'Application submitted, will update shortly'
                    ];
            }
        }

        return response()->json($data, $code);
	}

	public function documents(Request $request, $id)
	{
		$validator = Validator::make($request->all(), [
        	'file_type' => 'required',
            'file'      => 'required|mimes:jpeg,jpg,bmp,png,gif,pdf'
        ]);

        if ($validator->fails())
        {
        	foreach ($validator->errors()->all() as $error) 
	        {
	        	$code = 404;
	        	$data = [
	        		'code'     => $code,
	        		'response' => $error
	        	];
			}
        }
        else
        {
        	$user_id = $request->get('user_id');

        	$t = Transaction::whereHas('user',function ($query) use ($user_id) {
                    $query->where('id',$user_id);
                 })
        		 ->where('application_id', $id)
        		 ->first();

        	if (!is_null($t)) 
        	{
        		$accept = $this->canAccept($t->status);

                if ($accept['code'] != 200) 
                {
                    $code = 404;
                    $data = [
                            'code'     => $code,
                            'response' => $accept['status']
                        ];
                }
        		else
        		{
		        	$file = $request->file('file')->store('files','public');

		        	$t->upload()->updateOrCreate(
                        ['file_type' => strtoupper($request->file_type)],
                        ['file'      => $file]
                    );

                    $t->status = 'DOCUMENTS_UPLOADED';
                    $t->save();

		        	$code = 200;
			        $data = [
				        		'code'     => $code,
				        		'response' => 'File uploaded'
				        	];
        		}
        	}
        	else
        	{
        		$code = 404;
			    $data = [
				        	'code'     => $code,
				        	'response' => 'Invalid application'
						];
        	}
        }

        return response()->json($data, $code);
	}

	public function create(Request $request)
	{
		$validator = Validator::make($request->all(), [
        	'loan_type'              => 'required'
        ]);

        if ($validator->fails())
        {
        	foreach ($validator->errors()->all() as $error) 
	        {
	        	$code = 404;
	        	$data = [
	        		'code'     => $code,
	        		'response' => $error
	        	];
			}
        }
        else
        {
        	$u = User::find($request->get('user_id'));

        	$application_id = $this->generateApplicationId();
        	// Assigning application number

        	$u->transaction()->create([
        		'loan_type'      => $request->loan_type,
        		'status'         => 'INITIATED',
        		'application_id' => $application_id
        	]);

        	$code = 201;
	        $data = [
	        		'code'     => $code,
	        		'response' => 'Loan application created',
	        		'data'     => [
	        			'application_id' => $application_id
	        		]
	        	];
        }

        return response()->json($data, $code);
	}

    public function profile(Request $request, $id)
    {
    	$address_types   = 'required|in:permanent,current,rented,office,others';

        $validator = Validator::make($request->all(), [
            'first_name'             => 'bail|required|max:255',
            'last_name'              => 'bail|required|max:255',
            'dob'                    => 'bail|required|date|date_format:Y-m-d',
            'nominee_name'           => 'required',
            'nominee_relationship'   => 'required',
            'nominee_dob'            => 'bail|required|date|date_format:Y-m-d',
            'phone_number'           => 'bail|required|min:8|max:25',
            'secondary_number'       => 'bail|sometimes|min:8|max:25',
            'office.address_line_1'  => 'required',
            'office.address_line_2'  => 'required',
            'office.area'            => 'required',
            'office.street'          => 'required',
            'office.zipcode'         => 'required|min:6',
            'address.address_line_1' => 'required',
            'address.address_line_2' => 'required',
            'address.area'           => 'required',
            'address.street'         => 'required',
            'address.type'           => $address_types,
            'address.zipcode'        => 'required|min:6',
            'loan_amount'            => 'required|numeric',
            'tenure_in_months'       => 'required|numeric'
        ]);

        if ($validator->fails())
        {
        	foreach ($validator->errors()->all() as $error) 
	        {
	        	$code = 404;
	        	$data = [
	        		'code'     => $code,
	        		'response' => $error
	        	];
			}
        }
        else
        {
        	$user_id = $request->get('user_id');

        	$t = Transaction::whereHas('user',function ($query) use ($user_id) {
                    $query->where('id',$user_id);
                 })
        		 ->where('application_id', $id)
        		 ->first();

        	if (!is_null($t)) 
        	{
                $acceptable = $this->canAccept($t->status);

        		if ($acceptable['code'] != 200) 
        		{
        			$code = 406;
			        $data = [
				        		'code'     => $code,
				        		'response' => $acceptable['status']
				        	];
        		}
        		else
        		{
        			$t->status = 'PROFILE_SUBMITTED';
		        	$t->first_name   = ucfirst(strtolower($request->first_name));
		        	$t->last_name    = ucfirst(strtolower($request->last_name));
		        	$t->phone_number = $request->phone_number;
		        	$t->secondary_number = $request->secondary_number;
		        	$t->loan_amount = $request->loan_amount;
		        	$t->tenure_in_months = $request->tenure_in_months;
		        	$t->dob = $request->dob;
		        	$t->nominee_dob  = $request->nominee_dob;
		        	$t->nominee_name = ucwords(strtolower($request->nominee_name));
		        	$t->nominee_relationship = strtoupper($request->nominee_relationship);
		        	$t->save();

		        	$t->address()->delete();
		        	$t->address()->createMany([
		        		$request->address, // saving address
		        		$request->office   // saving office address
		        	]);

		        	$code = 201;
			        $data = [
				        		'code'     => $code,
				        		'response' => 'Profile details submitted'
				        	];
        		}
        	}
        	else
        	{
        		$code = 201;
		        $data = [
			        		'code'     => $code,
			        		'response' => 'Invalid loan application'
			        	];
        	}
        }

        return response()->json($data, $code);
    }


    public function employment(Request $request, $id)
    {
    	$employment_type = 'required|in:fulltime,contract,permanent,remote';

        $validator = Validator::make($request->all(), [
        	'employer_name'              => 'required',
        	'employment_type'  => $employment_type,
        	'job_role' => 'required',
        	'total_work_experience' => 'required|numeric',
        	'current_work_experience' => 'required|numeric',
        	'company_type' => 'required',
        	'bank_name' => 'required',
        	'bank_ifsc' => 'required',
        	'bank_account_number' => 'required',
        	'annual_income' => 'required|numeric',
        	'monthly_income' => 'required|numeric',
        	'credit_card_expense' => 'required|numeric',
        	'monthly_emi' => 'required|numeric',
        	'monthly_rent' => 'required|numeric'
        ]);

        if ($validator->fails())
        {
        	foreach ($validator->errors()->all() as $error) 
	        {
	        	$code = 404;
	        	$data = [
	        		'code'     => $code,
	        		'response' => $error
	        	];
			}
        }
        else
        {
        	$user_id = $request->get('user_id');

        	$t = Transaction::whereHas('user',function ($query) use ($user_id) {
                    $query->where('id',$user_id);
                 })
        		 ->where('application_id', $id)
        		 ->first();

        	if (!is_null($t)) 
        	{
        		$acceptable = $this->canAccept($t->status);

                if ($acceptable['code'] != 200) 
                {
                    $code = 406;
                    $data = [
                                'code'     => $code,
                                'response' => $acceptable['status']
                            ];
                }
        		else
        		{
        			$t->status = 'EMPLOYMENT_DETAILS_SUBMITTED';
	        		$t->employment_type = strtolower($request->employment_type);
	        		$t->employer_name   = ucwords(strtolower($request->employer_name));
	        		$t->job_role        = ucwords($request->job_role);
	        		$t->total_work_experience   = $request->total_work_experience;
	        		$t->current_work_experience = $request->current_work_experience;
	        		$t->company_type = ucwords(strtolower($request->company_type));
	        		$t->bank_name    = ucwords(strtolower($request->bank_name));
	        		$t->bank_ifsc    = strtoupper($request->bank_ifsc);
	        		$t->bank_account_number = $request->bank_account_number;
	        		$t->annual_income       = $request->annual_income;
	        		$t->monthly_income      = $request->monthly_income;
	        		$t->credit_card_expense = $request->credit_card_expense;
	        		$t->monthly_emi  = $request->monthly_emi;
	        		$t->monthly_rent = $request->monthly_rent;
	        		$t->save();
	 
	        		$code = 200;
			        $data = [
			        		'code'     => $code,
			        		'response' => 'Employment details submitted'
			        	];
        		}
        	}
        	else
        	{
        		$code = 404;
		        $data = [
		        		'code'     => $code,
		        		'response' => 'Invalid application'
		        	];
        	}        	
        }

        return response()->json($data, $code);
    }

    private function canAccept($status)
    {
        switch ($status) 
        {
               case 'APPLICATION_SUBMITTED':
                   
                    $response = [
                            'code'   => 406,
                            'status' => 'Application already submitted'
                        ];

                   break;

                case 'APPLICATION_CANCELLED':
                   
                    $response = [
                            'code'   => 406,
                            'status' => 'Loan application already cancelled'
                        ];

                   break;

                case 'LOAN_DISBURSED':
                   
                    $response = [
                            'code'   => 406,
                            'status' => 'Loan already disbursed'
                        ];

                   break;

                case 'APPLICATION_REJECTED':
                   
                    $response = [
                            'code'   => 406,
                            'status' => 'Application already rejected'
                        ];

                   break;

               case 'DOCUMENTS_APPROVED':
                   
                    $response = [
                            'code'   => 406,
                            'status' => 'Application already approved'
                        ];

                   break;
               
               default:

                   $response = [
                            'code'   => 200,
                            'status' => 'Acceptable'
                        ];

                   break;
        }   

        return $response;
    }


    private function generateApplicationId()
    {
    	$run = true;

    	while ($run) 
    	{
    		$application_id = strtoupper(str_random(8));

    		$exist = Transaction::where('application_id', $application_id)
    							->exists();

    		if (!$exist) 
    		{
    			$run = false;
    		}
    	}

    	return $application_id;
    }
}
