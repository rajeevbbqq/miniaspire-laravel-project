<?php

namespace Aspire\Http\Middleware;

use Closure;
use Aspire\Token;

class ValidateUser
{
    public function handle($request, Closure $next)
    {
        $bearer_token = $request->header('Authorization', null);

        if (is_null($bearer_token)) 
        {
            $code = 400;
            $data = [
                'code'     => $code,
                'response' => 'Bad request'
            ];
        }
        else 
        {
            $token = str_after($bearer_token, 'Bearer ');

            $t = Token::where([
                        'type'  => 'session',
                        'token' => $token
                 ])->with('user')
                 ->first();

            if (is_null($t)) 
            {
                $code = 401;
                $data = [
                    'code'     => $code,
                    'response' => 'Un-authorized'
                ];
            }
            else
            {
                $request->attributes->add([

                    'user_id'  => $t->user->id

                ]);

                return $next($request);
            }      
        }  

        return response()->json($data,$code);            
    }
}
