<?php

namespace Aspire;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $guarded = ['id', 'user_id'];

    protected $hidden = ['id', 'user_id'];

	protected $casts = [
    	    'nominee_dob' => 'date:Y-m-d',
    	            'dob' => 'date:Y-m-d'
	];
	
    public function user()
    {
    	return $this->belongsTo(User::class);
    }

    public function address()
    {
    	return $this->hasMany(Address::class);
    }

    public function upload()
    {
        return $this->hasMany(Upload::class);
    }

    public function loan()
    {
        return $this->hasOne(Loan::class);
    }
}
