<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function($table) {

            $table->date('dob')->after('last_name')->nullable();

            $table->string('nominee_relationship')->after('dob')->nullable();
            $table->string('nominee_name')->after('dob')->nullable();
            $table->date('nominee_dob')->after('dob')->nullable();

            $table->string('employer_name')->after('nominee_dob')->nullable();
            $table->enum('employment_type', ['fulltime', 'contract', 'permanent', 'remote'])->nullable()->after('nominee_dob');

            $table->string('job_role')->after('employer_name')->nullable();
            $table->integer('total_work_experience')->after('employer_name')->nullable();
            $table->integer('current_work_experience')->after('employer_name')->nullable();
            $table->string('company_type')->after('employer_name')->nullable();

            $table->string('bank_name')->after('job_role')->nullable();
            $table->string('bank_ifsc')->after('bank_name')->nullable();
            $table->string('bank_account_number')->after('bank_name')->nullable();
            
            $table->decimal('annual_income', 14,2)->after('bank_account_number')->nullable();
            $table->decimal('monthly_income', 12,2)->after('annual_income')->nullable();
            $table->decimal('credit_card_expense', 12,2)->nullable()->after('annual_income');
            $table->decimal('monthly_emi', 10,2)->nullable()->after('annual_income');
            $table->decimal('monthly_rent', 10,2)->nullable()->after('annual_income');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
