<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('transaction_id')->unsigned();

            $table->string('loan_number', 100)->unique();
            $table->decimal('applied_amount', 14,2);
            $table->decimal('approved_amount', 14,2);
            $table->decimal('disbursed_amount', 14,2);
            $table->decimal('processing_fee', 7,2);

            $table->decimal('interest', 5,2);
            $table->decimal('interest_payable', 14,2);
            $table->decimal('amount_payable', 14,2);

            $table->integer('tenure');
            $table->date('application_date');
            $table->date('loan_start_date');
            $table->date('loan_end_date');

            $table->timestamps();

            $table->foreign('transaction_id')->references('id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
