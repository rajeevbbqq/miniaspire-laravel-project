<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();

            $table->enum('type', [ 'permanent', 'current', 'rented', 'office', 'others'])
                  ->default('permanent');

            $table->mediumText('address_line_1');
            $table->mediumText('address_line_2')->nullable();
            $table->mediumText('address_line_3')->nullable();
            $table->string('area')->nullable();
            $table->string('street')->nullable();
            $table->string('zipcode', 7);
            $table->timestamps();

            $table->foreign('transaction_id')->references('id')->on('transactions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
}
