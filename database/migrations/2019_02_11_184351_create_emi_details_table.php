<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmiDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emi_details', function (Blueprint $table) {

            $table->increments('id');
            $table->integer('loan_id')->unsigned();
            $table->enum('is_paid', ['paid', 'unpaid'])->default('unpaid');
            $table->integer('term');
            $table->decimal('balance_amount', 16,2);
            $table->decimal('month_interest', 12,2);
            $table->decimal('total_interest', 14,2);
            $table->date('due_date');
            $table->timestamps();

            $table->foreign('loan_id')->references('id')->on('loans');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emi_details');
    }
}
